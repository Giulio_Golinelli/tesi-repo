*Minosse* (nome ispirato dal famoso personaggio dell'opera Dantesca) è un applicativo web, client-server, per il voto elettronico, la raccolta e il conteggio dei dati real-time e l'elezione di più candidati.  
Attualmente è reperibile online all'indirizzo: `193.70.2.109:3000`.
